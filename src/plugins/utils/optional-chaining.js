/**
 * Solve the problem that the optional chain cannot be used in the Vue Template
 * @param obj
 * @param rest
 * @returns {*}
 */
export const optionalChaining = (obj, ...rest) => {
  let tmp = obj
  for (let key in rest) {
    let name = rest[key]
    tmp = tmp?.[name]
  }
  return tmp || ""
}
