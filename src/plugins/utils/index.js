function isObject(o) {
  return o instanceof Object && o.constructor === Object
}

export { isObject }