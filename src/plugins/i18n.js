import Vue from "vue"
import VueI18n from "vue-i18n"
import { isObject } from "@/plugins/utils"
import { getStorageItemByKey, setStorageItemByKey } from "@/constants/utils/storage"

Vue.use(VueI18n)

const env_locale = getStorageItemByKey("i18n_locale") || process.env.VUE_APP_I18N_LOCALE || "uz"
const { langObject = {}, langPrefixes = [], messages = {} } = loadLocaleMessages()

function loadLocaleMessages() {
  const locales = require.context("@/locales", true, /[A-Za-z0-9-_,\s]+\.json$/i)
  const messages = {}
  locales.keys().forEach(key => {
    const matched = key.match(/([A-Za-z0-9-_]+)\./i)
    if (matched && matched.length > 1) {
      const locale = matched[1]
      messages[locale] = locales(key)
    }
  })

  // console.log("messages", messages);

  let langPrefixes = []
  let langObject = {}

  Object.keys(messages).forEach(key => {
    if (key) {
      langPrefixes = [...langPrefixes, key]
      langObject[key] = ""
    }
  })

  // console.log('langPrefixes', langPrefixes);

  return { langObject, langPrefixes, messages }
}

// console.log("langObject", langObject)
// console.log("langPrefixes", langPrefixes)
// console.log("messages", messages)

export function nt(obj, locale = i18n.locale) {
  if (!obj || !isObject(obj)) {
    console.warn("argument is not object")
    return "-"
  }

  if (obj[locale]) {
    return obj[locale]
  }

  if (obj[i18n.fallbackLocale]) {
    return obj[i18n.fallbackLocale]
  }

  const f = langPrefixes.find(lang_item => obj[lang_item])

  return obj[f] || "Не заполнено"
}

const i18n = new VueI18n({
  locale: env_locale,
  fallbackLocale: process.env.VUE_APP_I18N_FALLBACK_LOCALE || env_locale || "uz",
  messages
})

Vue.prototype.$langPrefixes = langPrefixes
Vue.prototype.$langProps = JSON.parse(JSON.stringify(langObject))
Vue.prototype.$nt = nt

if (process.env.NODE_ENV === "development") {
  console.log("i18n", i18n)
  console.log("i18n init lang", env_locale)
}

if (!getStorageItemByKey("i18n_locale")) {
  setStorageItemByKey("i18n_locale", env_locale)
}

export default i18n
