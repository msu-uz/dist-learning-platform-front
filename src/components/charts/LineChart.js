import { Line, mixins } from "vue-chartjs"
const { reactiveProp } = mixins

export default {
  extends: Line,
  mixins: [reactiveProp],
  props: ["options", "chartData"],
  data() {
    return {
      gradient: ""
      // gradient2: '',
      // htmlLegend: null
    }
  },
  mounted() {
    this.gradient = this.$refs.canvas.getContext("2d").createLinearGradient(0, 0, 0, 450)

    this.gradient.addColorStop(0, "rgba(111, 210 , 246, 0.5)")
    this.gradient.addColorStop(0.5, "rgba(111, 210 , 246, 0.1)")
    this.gradient.addColorStop(1, "rgba(111, 210 , 246, 0)")

    this.renderChart(
      {
        labels: ["2015 г.", "2016 г.", "2017 г.", "2018 г.", "2019 г.", "2020 г."],
        datasets: [
          {
            label: "Data One",
            borderColor: "#FC2525",
            pointBackgroundColor: "white",
            borderWidth: 1,
            pointBorderColor: "white",
            backgroundColor: this.gradient,
            data: [10, 20, 30, 15, 50, 40]
          }
        ]
      },
      this.options
    )

    // this.renderChart(this.chartData, this.options)
    // this.htmlLegend = this.generateLegend()
    // console.log('object', this.options, this.chartData);
  }
}
