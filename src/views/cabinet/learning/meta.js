import { EDUCATION_OFFICE_UNITS_LEVEL } from "@/constants/access_levels"

export const meta = {
  requiresAuth: true,
  access_level: [EDUCATION_OFFICE_UNITS_LEVEL]
}
