import { LIBRARIAN_LEVEL } from "@/constants/access_levels"

export const meta = {
  requiresAuth: true,
  access_level: [LIBRARIAN_LEVEL]
}
