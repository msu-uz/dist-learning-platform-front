import { TEACHER_LEVEL } from "@/constants/access_levels"

export const meta = {
  requiresAuth: true,
  access_level: [TEACHER_LEVEL]
}
