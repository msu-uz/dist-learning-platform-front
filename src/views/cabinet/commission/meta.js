import { ADMISSION_BOARD_LEVEL } from "@/constants/access_levels"

export const meta = {
  requiresAuth: true,
  access_level: [ADMISSION_BOARD_LEVEL]
}
