import { STUDENT_LEVEL } from "@/constants/access_levels"

export const meta = {
  requiresAuth: true,
  access_level: [STUDENT_LEVEL]
}
