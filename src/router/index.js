import Vue from "vue"
import VueRouter from "vue-router"
const routes = require("./routes")
import Store from "@/store"
import routeAccessList from "@/constants/route_access_list"

function lazyload(path) {
  return () => import(/* webpackChunkName: "[request]" */ `@/views/${path}`)
}

Vue.use(VueRouter)

const router = new VueRouter({
  mode: "history",
  routes: [
    {
      path: "/",
      component: () => import("@/layouts/default"),
      children: ((routes || {}).default || []).map(({ componentFilePath, ...props }) => ({
        ...props,
        component: lazyload(componentFilePath)
      }))
    },

    {
      name: "login",
      path: "/login",
      component: () => import("@/layouts/login-form")
    },

    {
      name: "authVerify",
      path: "/one-id-code",
      component: () => import("@/layouts/one-id-code")
    },

    {
      name: "refreshToken",
      path: "/refresh-token",
      component: () => import("@/layouts/refresh-token")
    },

    {
      name: "noAccess",
      path: "/403",
      component: () => import("@/layouts/403")
    },

    {
      name: "notFoundPage",
      path: "/404",
      component: () => import("@/layouts/404")
    },

    {
      path: "*",
      redirect: "/404"
    },

    {
      path: "**",
      redirect: "/404"
    }
  ]
})

router.beforeEach((to, from, next) => {
  const isAuthorized = !!Store.state.auth.access
  const accessLevel = ((Store.state.profile || {}).fields || {}).access_level || 0
  const accessGroup = (to.meta && to.meta.access_level) || []
  const requiresAuth = to && to.matched && to.matched.some(x => x.meta && x.meta.requiresAuth)

  const userRoleRoutes = (routeAccessList[accessLevel] || {}).routes || []

  const toRoute = (userRoleRoutes.length && userRoleRoutes[0]) || {}

  // console.log("profileProps", accessLevel);
  // console.log("routeAccessList", routeAccessList);
  // console.log("userRoleRoutes", userRoleRoutes);
  // console.log("accessGroup", accessGroup);
  // console.log("to", to.name || to.path);
  // console.log("toRoute", toRoute);
  // console.log("isAuthorized", isAuthorized);

  if (requiresAuth && !isAuthorized) {
    next({
      name: "login",
      query: {
        redirect: to.fullPath
      }
    })
    return
  } else if (isAuthorized) {
    // check is user have not access
    if (accessGroup.length && !accessGroup.includes(accessLevel)) {
      console.warn("Профиль не имеет доступа к данному роутеру")
      next({
        name: "noAccess"
      })
      // console.log("1-- noAccess");
      return
    }

    if (to.name == "login") {
      next({
        name: toRoute.name
      })
      // console.log("2--", toRoute.name);
      return
    }

    if (to.name != toRoute.name && (to.name == "index" || to.path == "/")) {
      next({
        name: toRoute.name
      })
      // console.log("3--", toRoute);
      return
    }
  }

  // console.log("4--", to.name || to.path);
  next()
})

router.afterEach(() => {
  const loader = document.getElementById("preloader-wrapper")

  if (loader) {
    loader.outerHTML = null
  }
})

export default router
