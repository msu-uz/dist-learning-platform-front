import { getToken as fGetToken } from "@/constants/utils/storage"

export const state = () => ({
  access: null,
  refresh: null
})

export const mutations = {
  setState(state, payload) {
    Object.keys(payload).forEach(key => {
      if (key in state) {
        state[key] = payload[key]
      }
    })
  }
}
export const actions = {
  init({ commit, dispatch }) {
    const token = fGetToken()
    const rtoken = fGetToken("refresh")

    const p = {
      access: token || null,
      refresh: rtoken || null
    }

    commit("setState", p)

    if (p.access) {
      dispatch("setCredentials", {
        data: {
          access: p.access,
          refresh: p.refresh
        }
      })
    }
    if (p.refresh) {
      dispatch("refreshToken")
    }
  },
  getToken({ dispatch }, params) {
    // console.log("getToken", params);
    return this.$axios
      .get("/auth/oauth/one-id/", {
        params
      })
      .then(res => {
        if (res && res.data && res.data.access && res.data.refresh) {
          dispatch("setCredentials", res)
        }

        return res
      })
  },
  setCredentials({ commit }, res) {
    const access_token = res?.data?.access
    const token_split = access_token.split(".")
    const fields_str = JSON.parse(atob(token_split[1]))

    commit("setState", res?.data)
    localStorage.setItem("tokens", JSON.stringify(res.data))

    //
    localStorage.setItem("whoami", fields_str.access_level)
    // console.log(fields_str)
    //
    commit(
      "profile/setState",
      {
        fields: fields_str
      },
      { root: true }
    )
  },
  refreshToken({ state, commit }) {
    // console.log("state", state);
    return this.$axios
      .post("/auth/refresh/", {
        refresh: state.refresh
      })
      .then(res => {
        // console.log("auth res", res.data);
        if (res.data && res.data.access) {
          commit("setState", {
            access: res.data.access,
            refresh: res.data.refresh || state.refresh
          })
          localStorage.setItem(
            "tokens",
            JSON.stringify({
              access: res.data.access,
              refresh: res.data.refresh || state.refresh
            })
          )
        }
      })
  },

  async login({ dispatch }, payload) {
    // console.log(payload);
    return this.$axios.post("/auth/access/", payload).then(res => {
      // console.log("setCredentials")
      return dispatch("setCredentials", res)
    })
  },

  logout({ commit }) {
    // console.log("logout");
    commit("setState", {
      access: null,
      refresh: null
    })
    localStorage.removeItem("tokens")
  }
}
