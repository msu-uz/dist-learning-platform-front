export const state = () => ({
  intervalList: []
})

export const mutations = {
  setState(state, payload) {
    Object.keys(payload).forEach(key => {
      if (key in state) {
        state[key] = payload[key]
      }
    })
  }
}

export const actions = {
  get(_, { key, params }) {
    let modalKey = ["discipline", "disciplinetopic", "int_control", "fin_control"].includes(key)
      ? key == "discipline"
        ? "/discipline"
        : `/discipline/${key == "disciplinetopic" ? "topic" : key}`
      : `/catalog/${key === "libraryelementcategory" ? `category` : key}`
    // console.log("Objec key", key, modalKey);
    if (!key) {
      modalKey = "/discipline"
    }

    return this.$axios.get(`${modalKey}/list/`, { params }).then(res => {
      console.log(key, res)
      if (res && res.data) {
        // commit("setState", { [params]: res.data });
        return res.data
      }
      return []
    })
  },

  getInterval({ commit }, { params }) {
    return this.$axios.get(`/schedule/event/interval/list/`, { params }).then(res => {
      if (res && res.data) {
        commit("setState", {
          intervalList: (res?.data?.results || [])
            .sort((a, b) => a.number - b.number)
            .map(item => ({
              title: this.$moment(item.start_time, "HH:mm:ss").format("HH:mm"),
              tab: item.number
            }))
        })
        return res.data
      }
      return []
    })
  },

  createOrUpdate(_, { payload, key }) {
    console.log("Key ", key)
    let modalKey = ["discipline", "disciplinetopic", "int_control", "fin_control"].includes(key)
      ? key == "discipline"
        ? "/discipline"
        : `/discipline/${key == "disciplinetopic" ? "topic" : key}`
      : `/catalog/${key === "libraryelementcategory" ? (payload.parentCategory ? `category/${payload.parentCategory}` : `category`) : `${key}`}`

    // let modalKey = key == 'discipline' ? '/discipline' : `/catalog/${key}`
    let editUrl = key === "libraryelementcategory" ? `${modalKey}/${payload.id}/update/` : `${modalKey}/update/${payload.id}/`
    let req = {
      url: payload && payload.id ? editUrl : `${modalKey}/create/`,
      method: payload && payload.id ? "put" : "post"
    }

    let formdata = new FormData()
    if (key == "city" || key == "district") {
      for (let key2 in payload) {
        if (key2 == "name") {
          let name = JSON.stringify(payload[key2])
          formdata.append(key2, name)
        } else formdata.append(key2, payload[key2])
      }
    }
    // console.log(key, "key", key == "city", formdata);

    return this.$axios[req.method](req.url, key == "city" || key == "district" ? formdata : payload.parentCategory ? { name: payload.name } : payload)
      .then(res => {
        // console.log("REQ", req, res)
        return res
      })
      .catch(err => {
        // console.log("Err", req, err)
        return err
      })
  },

  createOrUpdateInterval(_, payload) {
    let req = {
      url: payload && payload.id ? `/schedule/event/interval/${payload.id}/` : `/schedule/event/interval/create/`,
      method: payload && payload.id ? "put" : "post"
    }

    return this.$axios[req.method](req.url, payload)
      .then(res => {
        // console.log("REQ", req, res)
        return res
      })
      .catch(err => {
        console.log("Err", req, err)
        return err
      })
  },

  delete(_, { id, key }) {
    let modalKeyUrl = ["discipline", "disciplinetopic", "int_control", "fin_control"].includes(key)
      ? key == "discipline"
        ? `/discipline/delete/${id}/`
        : `/discipline/${key == "disciplinetopic" ? "topic" : key}/delete/${id}/`
      : `/catalog/${key}/destroy/${id}/`
    if (key === "libraryelementcategory") {
      modalKeyUrl = `/catalog/category/${id}/destroy/`
    }
    // let modalKeyUrl = key == 'discipline' ? `/discipline/delete/${id}/` : `/catalog/${key}/destroy/${id}/`

    return this.$axios
      .delete(modalKeyUrl)
      .then(res => {
        // console.log("del req", res)
        return res
      })
      .catch(err => {
        console.log("del err", err)
      })
  },

  deleteInterval(_, id) {
    return this.$axios
      .delete(`/schedule/event/interval/${id}/`)
      .then(res => {
        // console.log("del req", res)
        return res
      })
      .catch(err => {
        console.log("del err", err)
      })
  },

  fetchDepartments(_, { params }) {
    return this.$axios.get(`/discipline/department/list/`, { params }).then(res => {
      return res.data
    })
  },

  fetchCategoryParents(_, { params }) {
    return this.$axios
      .get(`/catalog/category/list/`, params)
      .then(res => {
        console.log(res.data)
        return res && res.data
      })
      .catch(err => {
        console.error(err)
      })
  },

  createCategory(_, { params }) {
    const url = params && params.parentCategory ? `${params.parentCategory}/create/` : `create/`
    return this.$axios.post(`/catalog/category/${url}`, params.name).then(res => {
      console.log(res)
    })
  },

  getChildrenOfParentCategory(_, { id }) {
    return this.$axios
      .get(`/catalog/category/${id}/`)
      .then(res => {
        return res
      })
      .catch(err => {
        return err
      })
  }
}
