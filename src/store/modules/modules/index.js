const actions = {
  fetchList(_, { groupId }) {
    return this.$axios.get(`/schedule/studentgroup/${groupId}/modulecourse/list/`)
  },

  weekTypeByModuleId(_, { disciplineId }) {
    return this.$axios.get(`/schedule/moduleweektype/list/?module_course_id=${disciplineId}`)
  },

  fetchRoomList(_, params) {
    return this.$axios.get("/schedule/room/list/", {
      params
    })
  }
}

export { actions }
