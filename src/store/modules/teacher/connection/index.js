export const state = () => ({
  count: null,
  results: []
})
export const mutations = {
  setState(state, payload) {
    Object.keys(payload).forEach(key => {
      if (key in state) {
        state[key] = payload[key]
      }
    })
  }
}
export const actions = {
  getModulesQuestion(_, { module_id, student_id }) {
    return this.$axios.get(`feedback/module/${module_id}/student/${student_id}/feedback/list/`)
  },
  getModules() {
    return this.$axios.get(`teacher/feedback/module/list/`)
  },

  getStatisticsList(_, id) {
    return this.$axios.get(`teacher/feedback/module/${id}/question/list/statistics/`).then(res => res && res.data)
  }
}
