export const actions = {
  getMaterialsLessonsList(_, params) {
    return this.$axios.get(`/teacher/material/lesson/list/`, { params }).then(res => (res && res.data && res.data.results) || [])
  },
  getCourseList(_, params) {
    return this.$axios.get(`/teacher/material/course/list/`, { params }).then(res => (res && res.data && res.data.results) || [])
  },
  getModuleList(_, params) {
    return this.$axios.get(`/teacher/material/module/list/?`, { params }).then(res => (res && res.data && res.data.results) || [])
  },
  getLessonMaterialsList(_, params) {
    return this.$axios.get(`/teacher/material/list/`, { params }).then(res => (res && res.data && res.data.results) || [])
  },
  getLessonMaterialsDetail(_, id) {
    return this.$axios.get(`/teacher/material/detail/${id}/`).then(res => (res && res.data) || {})
  },
  getTeacherList(_, params) {
    return this.$axios.get(`/teacher/material/teacher/list/`, { params }).then(res => (res && res.data && res.data.results) || [])
  },
  getStudentGroupList(_, params) {
    return this.$axios.get(`/teacher/material/student_group/list/`, { params }).then(res => (res && res.data && res.data.results) || [])
  },
  getStudentList(_, params) {
    return this.$axios.get(`/teacher/material/student/list/`, { params }).then(res => (res && res.data && res.data.results) || [])
  },
  sendOrUpdate(_, form) {
    const urlBase = `/teacher/material/`

    // eslint-disable-next-line

    const req = {
      method: form.id ? "patch" : "post",
      url: urlBase + ((form.id && `update/${form.id}/`) || "create/")
    }

    // console.log('shinima', form)

    if (typeof form.file === "string") delete form.file

    const f = new FormData()

    const { teacher, student, student_group, ...otherProps } = form

    Object.keys(otherProps).forEach(key => f.append(key, otherProps[key]))

    student_group.forEach(item => f.append("student_group", item))
    student.forEach(item => f.append("student", item))
    teacher.forEach(item => f.append("teacher", item))

    return this.$axios[req.method](req.url, f)
  },
  getMaterialQuestionList(_, params) {
    return this.$axios.get(`/teacher/material/question/list/`, { params }).then(res => res && res.data)
  },
  removeMaterial(_, id) {
    return this.$axios.delete(`/teacher/material/delete/${id}/`)
  }
}
