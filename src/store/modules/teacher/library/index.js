export const state = () => ({
  filterItem: {}
})
export const mutations = {
  setState(state, payload) {
    Object.keys(payload).forEach(key => {
      if (key in state) {
        state[key] = payload[key]
      }
    })
  }
}
export const actions = {
  getBookList(_, { media, params }) {
    return this.$axios.get(`/teacher/library/${media}/list/`, { params })
  },
  getFilter(_, filter) {
    return this.$axios.get(`/teacher/library/${filter}/list/`)
  },
  getFilterYear(_, params) {
    return this.$axios.get(`/teacher/library/year/list/`, { params }).then(res => res.data)
  },
  setFilter({ commit }, payload) {
    commit("setState", { filterItem: payload })
  },
  getStudents(_, params) {
    return this.$axios.get("/teacher/library/student/list/", { params }).then(res => res.data)
  },
  getStudentsGroup(_, params) {
    return this.$axios.get("/teacher/library/group/list/", { params }).then(res => res.data)
  },
  sendMailingVuex(_, { media, params }) {
    console.log(params)
    return this.$axios.post(`/teacher/library/${media}/share/`, params)
  },
  getMediaDetail(_, { media, id }) {
    return this.$axios.get(`/teacher/library/${media}/${id}/`).then(res => {
      return (res && res.data) || {}
    })
  },
  removeMedia(_, { id, media }) {
    return this.$axios.delete(`/teacher/library/${media}/${id}/`)
  }
}
