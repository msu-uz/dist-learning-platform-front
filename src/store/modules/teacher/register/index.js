const model = "/teacher/statement"

export const actions = {
  getModuleList(_, params) {
    return this.$axios.get(`${model}/module/list/`, { params }).then(res => {
      // console.log("Statement group detail", id, res);
      if (res && res.data) {
        return res.data
      }
      return []
    })
  },

  getModuleDetail(_, { id, params }) {
    return this.$axios.get(`${model}/module/${id}/`, { params }).then(res => {
      // console.log("Statement module", id, res);
      if (res && res.data) {
        return res.data
      }
      return []
    })
  },

  getModuleControl(_, id) {
    return this.$axios.get(`${model}/module/${id}/control/`).then(res => {
      // console.log("Statement module", id, res);
      if (res && res.data) {
        return res.data
      }
      return []
    })
  }
}
