const model = "/task"

export const actions = {
  getPermissions(_, params) {
    return this.$axios.get(`${model}/permissions/`, { params }).then(res => {
      // console.log(params, res);
      if (res && res.data) {
        return res.data
      }
      return []
    })
  },

  getList(_, params) {
    return this.$axios.get(`${model}/list/`, { params }).then(res => {
      // console.log(params, res);
      if (res && res.data) {
        return res.data
      }
      return []
    })
  },

  getDisciplineList(_, params) {
    return this.$axios.get(`${model}/discipline/list/`, { params }).then(res => {
      // console.log(params, res);
      if (res && res.data) {
        return res.data
      }
      return []
    })
  },

  getTopicList(_, params) {
    return this.$axios.get(`${model}/topic/list/`, { params }).then(res => {
      // console.log(params, res);
      if (res && res.data) {
        return res.data
      }
      return []
    })
  },

  getTypeList(_, params) {
    return this.$axios.get(`${model}/type/list/`, { params }).then(res => {
      // console.log(params, res);
      if (res && res.data) {
        return res.data
      }
      return []
    })
  },

  getTaskTypeList(_, params) {
    return this.$axios.get(`${model}/task_type/list/`, { params }).then(res => {
      // console.log(params, res);
      if (res && res.data) {
        return res.data
      }
      return []
    })
  },

  getDifficultyList(_, params) {
    return this.$axios.get(`${model}/difficulty/list/`, { params }).then(res => {
      // console.log(params, res);
      if (res && res.data) {
        return res.data
      }
      return []
    })
  },

  getDetail(_, id) {
    return this.$axios.get(`${model}/${id}/`).then(res => {
      // console.log("params", res);
      if (res && res.data) {
        return res.data
      }
      return []
    })
  },

  getHistory(_, id) {
    return this.$axios.get(`${model}/${id}/history/`).then(res => {
      // console.log("params", res);
      if (res && res.data) {
        return res.data
      }
      return []
    })
  },

  createTest(_, payload) {
    return this.$axios
      .post(`${model}/create/`, payload)
      .then(res => {
        // console.log("REQ", res);
        return res
      })
      .catch(err => {
        console.log("Err", err)
        return err
      })
  },

  updateTest(_, { id, payload }) {
    return this.$axios
      .patch(`${model}/${id}/`, payload)
      .then(res => {
        // console.log("REQ", res);
        return res
      })
      .catch(err => {
        console.log("Err", err)
        return err
      })
  },

  checkTest(_, { id, payload }) {
    return this.$axios
      .post(`${model}/${id}/check/`, payload)
      .then(res => {
        // console.log("REQ", res);
        return res
      })
      .catch(err => {
        console.log("Err", err)
        return err
      })
  },

  getStatisticsList(_, params) {
    return this.$axios.get(`${model}/statistics/list/`, { params }).then(res => {
      // console.log(params, res);
      if (res && res.data) {
        return res.data
      }
      return []
    })
  },

  getDiscipline(_, params) {
    return this.$axios.get(`${model}/discipline/list/`, { params }).then(res => {
      // console.log(params, res);
      if (res && res.data) {
        return res.data
      }
      return []
    })
  }
}
