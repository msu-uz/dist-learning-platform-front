export const actions = {
  getHomeTaskAnswerList(_, params) {
    return this.$axios.get(`/teacher/task/modulecourse/list/`, { params }).then(res => (res && res.data) || {})
  },
  getTaskModules(_, params) {
    return this.$axios.get(`/teacher/task_check/modules/list/`, { params }).then(res => (res && res.data) || {})
  },
  getHomeTaskAnswerDetail(_, { id, params }) {
    return this.$axios.get(`/teacher/task/${id}/hometask/list/`, { params }).then(res => (res && res.data) || [])
  },
  getTaskAnswerDetail(_, { id, params }) {
    return this.$axios.get(`/teacher/task_check/${id}/list/`, { params }).then(res => (res && res.data) || [])
  },
  getTaskCheckAnswerList(_, { id, subId, params }) {
    return this.$axios.get(`/teacher/task_check/${id}/${subId}/answers/list/`, { params }).then(res => (res && res.data) || [])
  },

  getTaskCheckStudentList(_, { id, params }) {
    return this.$axios.get(`/teacher/task_check/${id}/student/list/`, { params }).then(res => (res && res.data) || [])
  },

  sendOrUpdateTasks(_, form) {
    const urlBase = `/teacher/task_check/`

    // eslint-disable-next-line

    const req = {
      method: form.uniqId ? "patch" : "post",
      url: urlBase + ((form.uniqId && `task_answer_review_collection/${form.uniqId}/`) || "mark/create/")
    }

    return this.$axios[req.method](req.url, form)
  },
  sendOrUpdateHomeTasks(_, form) {
    const urlBase = `/teacher/task/hometaskreview/`

    // eslint-disable-next-line

    const req = {
      method: form.unqid ? "patch" : "post",
      url: urlBase + ((form.unqid && `${form.unqid}/update/`) || "create/")
    }

    return this.$axios[req.method](req.url, form)
  },
  removeMaterial(_, id) {
    return this.$axios.delete(`/teacher/material/delete/${id}/`)
  },

  getHomeTask(_, id) {
    return this.$axios.get(`/teacher/task/hometask/${id}/`).then(res => (res && res.data) || {})
  },

  getDetaiTaskmark(_, id) {
    return this.$axios.get(`/teacher/task_check/task_answer_review_collection/${id}/`).then(res => (res && res.data) || {})
  }
}
