const actions = {
  getTeacherScheduleByDate(_, { start_date, end_date }) {
    return this.$axios.get("/teacher/schedule/", {
      params: {
        start_date,
        end_date
      }
    })
  }
}

export { actions }
