const model = "/notification/template"

export const state = () => ({
  // count: 0,
  userList: []
  // list: [],
})

export const mutations = {
  setState(state, payload) {
    Object.keys(payload).forEach(key => {
      if (key in state) {
        state[key] = payload[key]
      }
    })
  }
}

export const actions = {
  getList(_, params) {
    return this.$axios.get(`${model}/list/`, { params }).then(res => {
      console.log(params, res)
      if (res && res.data) {
        return res.data
      }
      return []
    })
  },

  getUserList({ commit }, params) {
    return this.$axios.get(`/notification/user/list/`, { params }).then(res => {
      console.log("USers ", res)
      if (res && res.data) {
        commit("setState", {
          userList: res.data.results
        })
        return res.data
      }
      return []
    })
  },
  getUsersList(_, params) {
    return this.$axios.get(`/notification/user/list/`, { params }).then(res => {
      if (res && res.data) {
        return res.data
      }
      return []
    })
  },

  getDetail(_, id) {
    return this.$axios.get(`${model}/detail/${id}/`).then(res => {
      console.log("params", res)
      if (res && res.data) {
        return res.data
      }
      return []
    })
  },

  createTemplate(_, payload) {
    return this.$axios
      .post(`${model}/create/`, payload)
      .then(res => {
        console.log("REQ", res)
        return res
      })
      .catch(err => {
        console.log("Err", err)
        return err
      })
  },

  createNotification(_, payload) {
    return this.$axios
      .post(`/notification/create/`, payload)
      .then(res => {
        console.log("REQ", res)
        return res
      })
      .catch(err => {
        console.log("Err", err)
        return err
      })
  }
}
