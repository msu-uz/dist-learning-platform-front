const model = "/teacher/task"

export const actions = {
  getHometaskList(_, params) {
    return this.$axios.get(`${model}/list/`, { params }).then(res => {
      // console.log(params, res);
      if (res && res.data) {
        return res.data
      }
      return []
    })
  },

  getTestList(_, params) {
    return this.$axios.get(`${model}/taskcollection/list/`, { params }).then(res => {
      // console.log(params, res);
      if (res && res.data) {
        return res.data
      }
      return []
    })
  },

  getDetail(_, { id, endPoint }) {
    return this.$axios.get(`${model}/${endPoint}${id}/`).then(res => {
      // console.log("params", res);
      if (res && res.data) {
        return res.data
      }
      return []
    })
  },
  getTasksDetail(_, id) {
    return this.$axios.get(`${model}/${id}`).then(res => {
      // console.log('getTasksDetail', res)
      if (res && res.data) {
        return res.data
      }
      return []
    })
  },

  getModuleCourseList(_, params) {
    return this.$axios.get(`${model}/modulecourse/list/`, { params }).then(res => {
      // console.log(params, res);
      if (res && res.data) {
        return res.data
      }
      return []
    })
  },

  create(_, payload) {
    return this.$axios
      .post(`${model}/create/`, payload)
      .then(res => {
        // console.log("REQ", res);
        return res
      })
      .catch(err => {
        // console.log("Err", err);
        return err
      })
  },

  sendTask(_, { id, endPoint, payload }) {
    return this.$axios
      .post(`${model}/${endPoint}${id}/send/`, payload)
      .then(res => {
        // console.log("REQ", res);
        return res
      })
      .catch(err => {
        // console.log("Err", err);
        return err
      })
  },

  getStudents(_, params) {
    return this.$axios.get(`${model}/student/list/`, { params }).then(res => {
      // console.log("REQ", res);
      if (res && res.data) {
        return res.data
      }
      return []
    })
  },

  getStudentGroups(_, params) {
    return this.$axios.get(`${model}/group/list/`, { params }).then(res => {
      // console.log("REQ", res);
      if (res && res.data) {
        return res.data
      }
      return []
    })
  },
  chooseAction(_, { params, taskId }) {
    return this.$axios.patch(`${model}/taskcollection/${taskId}/`, params).then(res => {
      // console.log("REQ", res);
      if (res && res.data) {
        return res.data
      }
      return []
    })
  },
  getEventList(_, params) {
    return this.$axios.get(`${model}/event/list/`, { params }).then(res => {
      // console.log(res);
      return res.data
    })
  },
  getTestBase(_, params) {
    return this.$axios.get(`${model}/testbase/list/`, { params }).then(res => {
      // console.log("Test Base", res.data);
      return res.data
    })
  }
}
