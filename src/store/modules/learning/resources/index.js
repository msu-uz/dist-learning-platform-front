const model = "/resources"

export const actions = {
  getList(_, params) {
    return this.$axios.get(`${model}/list/`, { params }).then(res => {
      console.log(params, res)
      if (res && res.data) {
        return res.data
      }
      return []
    })
  },

  getResourceTypeList(_, params) {
    return this.$axios.get(`${model}/resourcestype/list/`, { params }).then(res => {
      console.log(params, res)
      if (res && res.data) {
        return res.data
      }
      return []
    })
  },

  deleteResources(_, id) {
    return this.$axios.delete(`${model}/${id}/destroy/`).then(res => {
      console.log("params", res)
      return res
    })
  },

  createOrUpdate(_, payload) {
    console.log("Before create or update", payload)
    let req = {
      url: payload.id ? `/${payload.id}/update/` : "/create/",
      method: payload.id ? "put" : "post"
    }

    return this.$axios[req.method](`${model}${req.url}`, payload)
      .then(res => {
        console.log("REQ", res)
        return res
      })
      .catch(err => {
        console.log("Err", err)
        return err
      })
  }
}
