export const state = () => ({
  count: null,
  results: []
})
export const mutations = {
  setState(state, payload) {
    Object.keys(payload).forEach(key => {
      if (key in state) {
        state[key] = payload[key]
      }
    })
  }
}
export const actions = {
  getListCourses({ commit }, params) {
    return this.$axios.get("/feedback/course/list/", { params }).then(res => {
      commit("setState", { count: res && res.data && res.data.count })
      return res && res.data && res.data.results
    })
  },
  getListStudent({ commit }, { id, params }) {
    return this.$axios.get(`/feedback/course/${id}/student/list/`, { params }).then(res => {
      commit("setState", { count: res && res.data && res.data.count })
      return res && res.data
    })
  },
  getGroupList(_, { id }) {
    return this.$axios.get(`/feedback/course/${id}/group/list/`).then(res => {
      return res && res.data && res.data && res.data.results
    })
  },

  getAllComments(_, { id, params }) {
    return this.$axios.get(`feedback/course/${id}/comment/list/`, { params })
  },

  getQuestion(_, { id, subId }) {
    return this.$axios.get(`feedback/course/${id}/student/${subId}/feedback/list/`)
  },
  getModulesQuestion(_, { module_id, student_id }) {
    return this.$axios.get(`feedback/module/${module_id}/student/${student_id}/feedback/list/`)
  },
  getModules(_, id) {
    return this.$axios.get(`feedback/course/${id}/module/list/`)
  },
  getComments(_, { id, subId, params }) {
    return this.$axios.get(`feedback/course/${id}/student/${subId}/comment/list/`, { params }).then(res => res && res.data)
  },
  getStatisticsList(_, { id }) {
    return this.$axios.get(`feedback/course/${id}/question/list/statistics/`).then(res => res && res.data)
  },

  createComment(_, payload) {
    return this.$axios.post("/feedback/comment/create/", payload)
  },

  getCourseQuestions({ commit }, course_id) {
    return this.$axios.get(`feedback/course/${course_id}/question/list/`).then(res => {
      commit("setState", { count: res && res.data && res.data.count })
      return res && res.data && res.data.results
    })
  },
  getTopics(_, id) {
    return this.$axios.get(`feedback/module/${id}/question/list/`).then(res => {
      // commit("setState", { count: res && res.data && res.data.count });
      return res && res.data
    })
  },
  createOrUpdateFeedbackQuestion(_, { form, courseOrModule }) {
    const urlBase = `/feedback/${courseOrModule}/question/`
    const req = {
      method: form.id ? "put" : "post",
      url: urlBase + ((form.id && `${form.id}/update/`) || "create/")
    }

    return this.$axios[req.method](req.url, form)
  },
  deleteFeedbackQuestion(_, { id, courseOrModule }) {
    return this.$axios.delete(`feedback/${courseOrModule}/question/${id}/delete/`)
  }
}
