const model = "/attendance"

export const actions = {
  getGroupList(_, params) {
    return this.$axios.get(`${model}/group/list/`, { params }).then(res => {
      console.log("Statement group", params, res)
      if (res && res.data) {
        return res.data
      }
      return []
    })
  },

  getCourseList(_, params) {
    return this.$axios.get(`${model}/course/list/`, { params }).then(res => {
      console.log("Statement group", params, res)
      if (res && res.data) {
        return res.data
      }
      return []
    })
  },

  getGroupDetail(_, { id, params }) {
    return this.$axios.get(`${model}/group/${id}/`, { params }).then(res => {
      console.log("Statement group detail", id, res)
      if (res && res.data) {
        return res.data
      }
      return []
    })
  }
}
