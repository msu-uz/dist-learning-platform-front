export const actions = {
  getLoadListToday() {
    return this.$axios.get(`/load/today/`).then(res => {
      return (res && res.data && res.data.results) || []
    })
  },
  getLoadListMonth() {
    return this.$axios.get(`/load/month/`).then(res => {
      return (res && res.data && res.data.results) || []
    })
  },
  getLoadListPeriod(_, params) {
    return this.$axios.get(`/load/period/`, { params }).then(res => {
      return (res && res.data && res.data.results) || []
    })
  }
}
