const model = "/statement"

export const actions = {
  getGroupList(_, params) {
    return this.$axios.get(`${model}/group/list/`, { params }).then(res => {
      // console.log("Statement group", params, res);
      if (res && res.data) {
        return res.data
      }
      return []
    })
  },

  getCourseList(_, params) {
    return this.$axios.get(`${model}/course/list/`, { params }).then(res => {
      // console.log("Statement group", params, res);
      if (res && res.data) {
        return res.data
      }
      return []
    })
  },

  getGroupDetail(_, { id, params }) {
    return this.$axios.get(`${model}/group/${id}/`, { params }).then(res => {
      // console.log("Statement group detail", id, res);
      if (res && res.data) {
        return res.data
      }
      return []
    })
  },

  getModuleDetail(_, { id, params }) {
    return this.$axios.get(`${model}/module/${id}/`, { params }).then(res => {
      // console.log("Statement module", id, res);
      if (res && res.data) {
        return res.data
      }
      return []
    })
  },

  getModuleControl(_, id) {
    return this.$axios.get(`${model}/module/${id}/control/`).then(res => {
      // console.log("Statement module", id, res);
      if (res && res.data) {
        return res.data
      }
      return []
    })
  },

  create(_, payload) {
    return this.$axios
      .post(`${model}/create/`, payload)
      .then(res => {
        // console.log("REQ", res);
        return res
      })
      .catch(err => {
        console.log("Err", err)
        return err
      })
  },

  expelStudent(_, { id, payload }) {
    return this.$axios
      .put(`${model}/student/${id}/`, payload)
      .then(res => {
        // console.log("REQ", res);
        return res
      })
      .catch(err => {
        console.log("Err", err)
        return err
      })
  },

  retakeCreate(_, payload) {
    return this.$axios
      .post(`${model}/retake/create/`, payload)
      .then(res => {
        // console.log("REQ", res);
        return res
      })
      .catch(err => {
        console.log("Err", err)
        return err
      })
  }
}
