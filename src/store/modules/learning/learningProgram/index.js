export const state = () => ({
  count: 0,
  results: []
})

export const mutations = {
  setState(state, payload) {
    Object.keys(payload).forEach(key => {
      if (key in state) {
        state[key] = payload[key]
      }
    })
  },
  removeState(state, id) {
    const f = state.results.findIndex(item => item.id == id)
    if (f != -1) {
      state.results = [...state.results.slice(0, f), ...state.results.slice(f + 1)]
    }
  }
}

export const actions = {
  //course

  getCourseList({ commit }, { search, params }) {
    return this.$axios
      .get(`/discipline/course/list/${search ? "?search=" + search : ""}`, {
        params
      })
      .then(res => {
        commit("setState", { count: res && res.data && res.data.count })
        return (res.data && res.data.results) || [] || []
      })
  },

  getDetailCourse(_, id) {
    return this.$axios.get(`/discipline/course/${id}/`).then(res => {
      return (res && res.data) || {}
    })
  },

  removeCourse(_, id) {
    return this.$axios.delete(`/discipline/course/${id}/`)
  },

  //(create || update) course

  createOrUpdateCourse(_, form) {
    const urlBase = "/discipline/course/"

    // eslint-disable-next-line
    const { created_at, updated_at, ...props } = form

    const req = {
      method: props.id ? "put" : "post",
      url: urlBase + ((props.id && `${props.id}/`) || "create/")
    }

    const f = new FormData()

    Object.keys(props).forEach(key => {
      if (key == "page_count") f.append(key, +props[key])
      else f.append(key, props[key])
    })

    return this.$axios[req.method](req.url, f)
  },

  //module

  getModuleList(_, { search, course_id, params }) {
    return this.$axios
      .get(`/discipline/course/${course_id}/modulecourse/list/${search ? "?search=" + search : ""}`, {
        params
      })
      .then(res => {
        if (res && res.data) {
          return (res.data && res.data.results) || [] || []
        }
      })
  },

  getModuleDetail(_, { course_id, id, params }) {
    return this.$axios
      .get(`/discipline/course/${course_id}/modulecourse/${id}/`, {
        params
      })
      .then(res => {
        return (res && res.data) || {}
      })
  },

  //(create || update) module

  createOrUpdateModule(_, { course_id, form }) {
    const urlBase = `/discipline/course/${course_id}/modulecourse/`

    // eslint-disable-next-line

    const req = {
      method: form.id ? "put" : "post",
      url: urlBase + ((form.id && `${form.id}/`) || "create/")
    }

    const f = new FormData()

    Object.keys(form).forEach(key => {
      if (key == "page_count") f.append(key, +form[key])
      else f.append(key, form[key])
    })

    return this.$axios[req.method](req.url, f)
  },

  getCourseTypeList(_, params) {
    return this.$axios
      .get(`/discipline/course_type/list/`, {
        params
      })
      .then(res => {
        if (res && res.data) {
          return (res.data && res.data.results) || []
        }
      })
  },

  getDisciplineList(_, params) {
    return this.$axios
      .get(`/discipline/list/`, {
        params
      })
      .then(res => {
        return res && res.data
      })
  },

  getModuleRead(_, { id, params }) {
    return this.$axios
      .get(`/discipline/modulecourse/${id}/`, {
        params
      })
      .then(res => {
        return (res && res.data) || []
      })
  },

  removeModule(_, { course_id, id }) {
    return this.$axios.delete(`/discipline/course/${course_id}/modulecourse/${id}/`)
  },

  // Course week list

  getCourseWeekList(_, { id, params }) {
    return this.$axios
      .get(`/discipline/modulecourse/${id}/week/list/`, {
        params
      })
      .then(res => {
        return (res && res.data && res.data.results) || []
      })
  },

  // Course week detail
  getCourseWeekDetail(_, { id }) {
    return this.$axios.get(`/discipline/week/${id}/`).then(res => {
      return (res && res.data) || {}
    })
  },

  // Add week
  createCourseWeekList(_, { id, form }) {
    return this.$axios.post(`/discipline/modulecourse/${id}/week/create/`, form)
  },

  // Update week
  updateCourseWeekList(_, { id, form }) {
    return this.$axios.patch(`/discipline/week/${id}/`, form)
  },

  getCoursTopicList({ commit }, params) {
    return this.$axios
      .get(`/discipline/topic/list/`, {
        params
      })
      .then(res => {
        commit("setState", { count: res.data.count })
        return ((res && res.data && res.data.results) || []).map(item => {
          return {
            ...item,
            checked: false
          }
        })
      })
  },
  getCoursIntControlList(_, params) {
    return this.$axios
      .get(`/discipline/int_control/list/`, {
        params
      })
      .then(res => {
        return (res && res.data && res.data.results) || []
      })
  },
  getCoursFinControlList(_, params) {
    return this.$axios
      .get(`/discipline/fin_control/list/`, {
        params
      })
      .then(res => {
        return (res && res.data && res.data.results) || []
      })
  },
  getTopics(_, { id, subId, title }) {
    return this.$axios.get(`/discipline/week/${id}/${title}/${subId}/add_topic/`).then(res => {
      return (res && res.data) || []
    })
  },
  addTopics(_, { id, subId, topicsArr, title }) {
    return this.$axios.patch(`/discipline/week/${id}/${title}/${subId}/add_topic/`, JSON.stringify(topicsArr)).then(res => {
      return (res && res.data) || []
    })
  },
  setOrder(_, { params, id }) {
    return this.$axios.patch(`/education-office/teacher/${id}/edit/`, params).then(res => {
      return res.data
    })
  }
}
