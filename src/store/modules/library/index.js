export const actions = {
  getBookList(_, { media, params }) {
    return this.$axios.get(`/library/${media}/list/`, { params })
  },
  getFilter(_, filter) {
    return this.$axios.get(`/library/${filter}/list/`)
  },
  getFilterYear(_, params) {
    return this.$axios.get(`/library/year/list/`, { params }).then(res => res.data)
  },
  setFilter({ commit }, payload) {
    commit("setState", { filterItem: payload })
  },

  getMediaDetail(_, { media, id }) {
    return this.$axios.get(`/library/${media}/${id}/`).then(res => {
      return (res && res.data) || {}
    })
  },
  removeMedia(_, { id, media }) {
    return this.$axios.delete(`/library/${media}/${id}/`)
  }
}
