export const state = () => ({
  count: 0,
  results: []
})

export const mutations = {
  setState(state, payload) {
    Object.keys(payload).forEach(key => {
      if (key in state) {
        state[key] = payload[key]
      }
    })
  },
  removeState(state, id) {
    const f = state.results.findIndex(item => item.id == id)
    if (f != -1) {
      state.results = [...state.results.slice(0, f), ...state.results.slice(f + 1)]
    }
  }
}

export const actions = {
  //Teacher list

  getTeacherList({ commit }, params) {
    return this.$axios
      .get("/education-office/teacher/list/", {
        params
      })
      .then(res => {
        if (res && res.data) {
          commit("setState", res.data || [])
        }
      })
  },

  uploadTeacher(_, form) {
    const urlBase = "/education-office/teacher/"

    // eslint-disable-next-line
    const { created_at, updated_at, ...props } = form;

    const req = {
      method: props.id ? "put" : "post",
      url: urlBase + ((props.id && `${props.id}/edit/`) || "create/")
    }

    const f = new FormData()

    Object.keys(props).forEach(key => {
      if (key == "page_count") f.append(key, +props[key])
      else f.append(key, props[key])
    })

    return this.$axios[req.method](req.url, f)
  },

  getTeacherDetail(_, id) {
    return this.$axios.get(`/education-office/teacher/${id}/`).then(res => {
      return (res && res.data) || {}
    })
  },

  removeTeacher({ commit }, id) {
    return this.$axios.delete(`/education-office/teacher/${id}/edit/`).then(() => commit("removeState", id))
  },

  updateTeacherDiscipline(_, { id, payload }) {
    return this.$axios.patch(`/education-office/teacher/${id}/edit/`, payload)
  }
}
