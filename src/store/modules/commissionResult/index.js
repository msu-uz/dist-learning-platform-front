export const state = () => ({
  count: 0,
  results: [],
  admitted: {},
  userFields: {},
  id: null
})

export const mutations = {
  setState(state, payload) {
    Object.keys(payload).forEach(key => {
      if (key in state) {
        state[key] = payload[key]
      }
    })
  },
  removeState(state, id) {
    const f = state.results.findIndex(item => item.id == id)
    if (f != -1) {
      state.results = [...state.results.slice(0, f), ...state.results.slice(f + 1)]
    }
  }
}
export const actions = {
  // Results

  getResultsList({ commit }, params) {
    return this.$axios
      .get("/admission_board/applicants/results/list/", {
        params
      })
      .then(res => {
        if (res && res.data) {
          commit("setState", res.data)
        }
      })
  },

  generatePassedList(_, count) {
    return this.$axios.post("/admission_board/applicants/generate-passed-list/", count).then(res => {
      if (res && res.data) {
        console.log("Passed list was generated successfully")
      }
    })
  }
}
