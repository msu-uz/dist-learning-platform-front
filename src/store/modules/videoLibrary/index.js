export const state = () => ({
  count: 0,
  results: []
})

export const mutations = {
  setState(state, payload) {
    Object.keys(payload).forEach(key => {
      if (key in state) {
        state[key] = payload[key]
      }
    })
  },
  removeState(state, id) {
    const f = state.results.findIndex(item => item.id == id)
    if (f != -1) {
      state.results = [...state.results.slice(0, f), ...state.results.slice(f + 1)]
    }
  }
}

export const actions = {
  //Books

  getVideoList({ commit }, params) {
    return this.$axios
      .get("/library/video/list", {
        params
      })
      .then(res => {
        if (res && res.data) {
          commit("setState", res.data || [])
        }
      })
  },

  createVideo() {
    return this.$axios.post("/library/video/create").then(() => console.log("The video created successfully!"))
  },

  removeVideo({ commit }, id) {
    return this.axios.delete(`/library/video/${id}`).then(() => commit("removeState"))
  }
}
