export const actions = {
  getListCourses(_, { params }) {
    return this.$axios.get(`/student/feedback/course/list/`, params)
  },
  getListModules(_, { id, params }) {
    return this.$axios.get(`student/feedback/course/${id}/module/list/`, {
      params
    })
  },
  getAllComments(_, { id }) {
    return this.$axios.get(`/student/feedback/course/${id}/comment/list/`).then(res => res.data && res.data.results)
  },
  createComments(_, { message }) {
    return this.$axios.post(`student/feedback/comment/create/`, message)
  },
  getQuestions(_, { m_c, id }) {
    return this.$axios.get(`/student/feedback/list/${m_c}/${id}/`).then(res => res.data && res.data.results)
  },
  sendAns(_, { m_c, answers }) {
    return this.$axios.post(`/student/feedback/create/${m_c}/`, { answers })
  }
}
