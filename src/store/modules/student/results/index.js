export const actions = {
  getStudentsResultsList(_, params) {
    return this.$axios.get(`/student/results/list/`, { params }).then(res => (res && res.data) || [])
  },
  getStudentResultsSummaryList(_, id) {
    return this.$axios.get(`/student/results/summary/${id}/list/`).then(res => (res && res.data) || [])
  },
  getStudentResultsModulesList(_, id) {
    return this.$axios.get(`/student/results/modules/${id}/list/`).then(res => (res && res.data) || [])
  }
}
