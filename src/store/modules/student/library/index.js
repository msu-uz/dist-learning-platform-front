export const actions = {
  getBookListLib(_, { media, params }) {
    return this.$axios.get(`/student/library/${media}/list/`, { params })
  },
  getFilterLib(_, filter) {
    return this.$axios.get(`/student/library/${filter}/list/`)
  },
  getFilterYearLib(_, params) {
    return this.$axios.get(`/student/library/year/list/`, { params }).then(res => res.data)
  },

  getStudentsLib(_, params) {
    return this.$axios.get("/student/library/student/list/", { params }).then(res => res.data)
  },
  getStudentsGroupLib(_, params) {
    return this.$axios.get("/student/library/group/list/", { params }).then(res => res.data)
  },

  getMediaDetailLib(_, { media, id }) {
    return this.$axios.get(`/student/library/${media}/${id}/`).then(res => {
      return (res && res.data) || {}
    })
  },
  getLinks(_, media) {
    return this.$axios.get(`/student/library/${media}/share/list/`).then(res => res.data && res.data.results)
    // student/library/videomaterial/share/list/?module=int
  }
}
