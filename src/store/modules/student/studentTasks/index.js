const model = "/student/task"

export const actions = {
  getDetail(_, id) {
    return this.$axios.get(`${model}/collection/${id}/`).then(res => {
      // console.log(params, res);
      if (res && res.data) {
        return res.data
      }
      return []
    })
  },
  getTaskCollection(_, params) {
    return this.$axios.get(`${model}/collection/list/`, { params }).then(res => {
      // console.log(params, res);
      if (res && res.data) {
        return res.data
      }
      return []
    })
  },
  getModuleList(_, params) {
    return this.$axios.get(`${model}/results/module/list/`, { params }).then(res => {
      return res.data
    })
  },
  getResultList(_, params) {
    return this.$axios.get(`${model}/results/collection/list/`, { params }).then(res => {
      return res.data
    })
  },
  getResultDetail(_, id) {
    return this.$axios.get(`${model}/results/collection/${id}/`).then(res => {
      return res.data
    })
  },
  sendAnswer(_, { task_answer_collection, task, chosen_variant, plain_answer }) {
    if (chosen_variant) {
      return this.$axios
        .post(`${model}/answer/create/`, {
          task_answer_collection,
          task,
          chosen_variant
        })
        .then(res => {
          return res.data
        })
    } else if (plain_answer) {
      return this.$axios
        .post(`${model}/answer/create/`, {
          task_answer_collection,
          task,
          plain_answer
        })
        .then(res => {
          return res.data
        })
    }
  },
  updateAnswer(_, { id, chosen_variant, plain_answer }) {
    if (chosen_variant) {
      return this.$axios.patch(`${model}/answer/${id}/`, { chosen_variant }).then(res => {
        return res.data
      })
    } else if (plain_answer) {
      return this.$axios.patch(`${model}/answer/${id}/`, { plain_answer }).then(res => {
        return res.data
      })
    }
  }
}
