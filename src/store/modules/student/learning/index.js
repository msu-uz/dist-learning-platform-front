const model = "/student/task/hometask"

export const actions = {
  getList(_, params) {
    return this.$axios.get(`${model}/list/`, { params }).then(res => {
      console.log(params, res)
      if (res && res.data) {
        return res.data
      }
      return []
    })
  },
  getScoreList(_, params) {
    return this.$axios.get(`${model}/list/`, { params }).then(res => {
      console.log(params, res)
      if (res && res.data) {
        return res.data
      }
      return []
    })
  },
  getDetail(_, id) {
    return this.$axios.get(`${model}/${id}/`).then(res => {
      console.log(res)
      if (res && res.data) {
        return res.data
      }
      return {}
    })
  },
  createAnswer(_, payload) {
    return this.$axios.post(`${model}/answer/create/`, payload).then(res => {
      console.log(res)
      res ? res : {}
    })
  },
  getHomeTaskDetail(_, id) {
    return this.$axios.get(`${model}/${id}/`).then(res => {
      return (res && res.data) || {}
    })
  }
}
