export const actions = {
  getAccount(_, params) {
    return this.$axios.get(`/student/dashboard/account/`, { params }).then(res => (res && res.data) || {})
  },
  getStudentCourseList(_, params) {
    return this.$axios.get(`/student/dashboard/course/list/`, { params }).then(res => (res && res.data) || [])
  },
  getStudentSchedule(_, params) {
    return this.$axios.get(`/student/dashboard/schedule/`, { params }).then(res => (res && res.data) || [])
  },
  getStudentMaterials(_, params) {
    return this.$axios.get(`/student/dashboard/material/list/`, { params }).then(res => (res && res.data) || [])
  }
}
