const actions = {
  getStudentScheduleByDate(_, { start_date, end_date }) {
    return this.$axios.get("/student/schedule/", {
      params: {
        start_date,
        end_date
      }
    })
  }
}

export { actions }
