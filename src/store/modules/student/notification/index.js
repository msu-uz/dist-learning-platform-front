const model = "/student/notification"

export const actions = {
  getList(_, params) {
    return this.$axios.get(`${model}/list/`, { params }).then(res => {
      //   console.log("Notifications -> ", res);
      return res && res.data ? res.data : []
    })
  },
  getModuleList(_, params) {
    return this.$axios.get("/student/task/module/list/", { params }).then(res => {
      return res.data
    })
  }
}
