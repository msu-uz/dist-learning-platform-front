export const state = () => ({
  toasts: []
})
export const mutations = {
  changeToasts(state, payload) {
    if (!payload.id) {
      payload.id = Math.random()
        .toString()
        .slice(2)
      payload.off = false
      state.toasts.push(payload)
    } else {
      const t = state.toasts.findIndex(item => item.id == payload.id)
      if (t != -1) {
        // state.toasts[t].off = true;
        // setTimeout(() => {
        // state.toasts.splice(t, 1);
        // 1 [2] 3
        state.toasts = [...state.toasts.slice(0, t), ...state.toasts.slice(t + 1)]
        // }, 200);
      }
    }
  }
}
export const actions = {
  changeToasts({ commit }, payload) {
    commit("changeToasts", payload)
  }
}
