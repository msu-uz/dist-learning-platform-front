// export const state = () => ({
//   count: 0,
//   groups: [],
//   students: [],
//   listStudents: [],
//   studentsInGroup: []
// });
// export const mutations = {
//   setState(state, payload) {
//     Object.keys(payload).forEach(key => {
//       if (key in state) {
//         state[key] = payload[key];
//       }
//     });
//   },
//   removeState(state, id) {
//     const f = state.groups.findIndex(item => item.id == id);
//     if (f != -1) {
//       state.groups = [
//         ...state.groups.slice(0, f),
//         ...state.groups.slice(f + 1)
//       ];
//     }
//   },
//   removeStudent(state, id) {
//     const f = state.students.findIndex(item => item.id == id);
//     if (f != -1) {
//       state.students = [
//         ...state.students.slice(0, f),
//         ...state.students.slice(f + 1)
//       ];
//     }
//   },
// };
export const actions = {
  getGroupList(_, params) {
    console.log(params)
    return this.$axios.get("/education-office/student-group/list/", params)
  },
  changeGroup(_, { group_id, action, params }) {
    return this.$axios[action](`/education-office/student-group/${group_id}/edit/`, params)
  },
  deleteStudent(_, { idGroup, idStudent }) {
    return this.$axios.delete(`/education-office/student-group/${idGroup}/student/${idStudent}/delete/`)
  },
  getFullTimeStudents(_, params) {
    return this.$axios.get("/education-office/student/list/", { params })
  },
  sendId(_, payload) {
    return this.$axios.post("/education-office/student-group/create/", payload)
  },
  getStudents(_, { id, params }) {
    return this.$axios.get(`/education-office/student-group/${id}/student/`, {
      params
    })
  },
  getGroupWithStudents(_, id) {
    return this.$axios.get(`/education-office/student-group/${id}/`)
  },
  getCathedra() {
    return this.$axios.get("education-office/department/list/")
  },
  getInfoStudent(_, { id, idStudent }) {
    return this.$axios.get(`/education-office/student-group/${id}/student/${idStudent}/`)
  },
  getStudentEmployeeList(_, params) {
    return this.$axios.get(`/education-office/student/list/`, { params }).then(res => {
      console.log("Statement group", params, res)
      if (res && res.data) {
        return res.data
      }
      return []
    })
  },
  getCity(_, params) {
    return this.$axios.get(`/education-office/city/list/`, { params }).then(res => {
      console.log("Statement group", params, res)
      if (res && res.data) {
        return res.data
      }
      return []
    })
  },
  getDistrictByCity(_, params) {
    return this.$axios.get(`/education-office/district/list/`, { params }).then(res => {
      console.log("Statement group", params, res)
      if (res && res.data) {
        return res.data
      }
      return []
    })
  },
  getOrganizationByCity(_, params) {
    return this.$axios.get(`/education-office/organization/list/`, { params }).then(res => {
      console.log("Statement group", params, res)
      if (res && res.data) {
        return res.data
      }
      return []
    })
  },
  getPosition(_, params) {
    return this.$axios.get(`/education-office/position/list/`, { params }).then(res => {
      if (res && res.data) {
        return res.data
      }
      return []
    })
  },
  getTeacherGroups(_, { params, id }) {
    if (id) {
      return this.$axios.get(`/teacher/group/${id}/`, { params })
    }
    return this.$axios.get("/teacher/group/list/", { params })
  },

  getPerformance(_, { group_id, student_id, params }) {
    return this.$axios.get(`/education-office/student-group/${group_id}/student/${student_id}/performance/`, { params }).then(res => {
      if (res && res.data) {
        return res.data
      }
      return []
    })
  },

  getPerformanceControl(_, { group_id, student_id, params }) {
    return this.$axios.get(`/education-office/student-group/${group_id}/student/${student_id}/performance/control/`, { params }).then(res => {
      if (res && res.data) {
        return res.data
      }
      return []
    })
  }
  // editGroup(_, { params, id }) {
  //   console.log(params);
  //   return this.$axios
  //     .put(`education-office/student-group/${id}/edit/`, params)
  //     .then(res => {
  //       console.log(res);
  //     });
  // }
}
