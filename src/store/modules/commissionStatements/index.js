export const state = () => ({
  count: 0,
  results: [],
  admitted: {},
  userFields: {},
  id: null
})

export const mutations = {
  setState(state, payload) {
    Object.keys(payload).forEach(key => {
      if (key in state) {
        state[key] = payload[key]
      }
    })
  }
}
export const actions = {
  //Applicants

  getApplicantsList({ commit }, params) {
    return this.$axios
      .get("/admission_board/applicants/list/", {
        params
      })
      .then(res => {
        if (res && res.data) {
          commit("setState", res.data)
        }
      })
  },
  getApplicantsDetail({ commit }, id) {
    return this.$axios.get(`/admission_board/applicants/${id}/`).then(res => {
      if (res && res.data) {
        commit("setState", { userFields: res.data })
        console.log("detail info", res.data)
      }
    })
  },
  getApplicantsDetailToGroupList({ commit }, { id, subId }) {
    return this.$axios.get(`/education-office/student-group/${id}/student/${subId}/`).then(res => {
      if (res && res.data) {
        commit("setState", { userFields: res.data })
        console.log("detail info", res.data)
      }
    })
  },
  approveCreate({ commit }, id) {
    return this.$axios.post(`/admission_board/applicants/approve/${id}/`).then(res => {
      if (res && res.data) {
        commit("setState", res.data)
      }
    })
  },
  declineCreate({ commit }, id) {
    return this.$axios.post(`/admission_board/applicants/decline/${id}/`).then(res => {
      if (res && res.data) {
        commit("setState", res.data)
      }
    })
  }
}
