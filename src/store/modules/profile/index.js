export const state = () => ({
  fields: {},
  role: ""
})

export const mutations = {
  setState(state, payload) {
    Object.keys(payload).forEach(key => {
      if (key in state) {
        state[key] = payload[key]
        state.role = state[key].access_level == 1 ? "student" : state[key].access_level == 2 ? "teacher" : "library"
      }
    })
  }
}
