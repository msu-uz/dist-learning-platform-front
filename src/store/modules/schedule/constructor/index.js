const actions = {
  fetchListByModule(_, moduleId) {
    return this.$axios.get(`/schedule/constructor/group/${moduleId}/module/list/`)
  },

  fetchShortmoduleListByGroupId(_, { groupId }) {
    return this.$axios.get(`/schedule/constructor/group/${groupId}/shortmodule/list/`)
  },

  fetchModuleIfoById(_, { moduleId }) {
    return this.$axios.get(`/schedule/constructor/module/${moduleId}/info/`)
  },

  fetchEventListByModuleScheduleGroupId(_, { groupId, params }) {
    /*
      @params: date
    */
    return this.$axios.get(`/schedule/constructor/group/${groupId}/event/list/`, { params })
  },

  setModuleTeacher(_, { moduleId, teacher }) {
    return this.$axios.patch(`/schedule/constructor/module/${moduleId}/update/`, { teacher })
  },

  validateEvent() {
    return this.$axios.post("/schedule/constructor/event/validate/")
  },

  submitConstructorForm(_, payload) {
    try {
      const { room, teacher, module, topic } = payload
      const req = {
        type: payload.id ? "patch" : "post",
        url: payload.id ? `/schedule/constructor/event/${payload.id}/update/` : "/schedule/constructor/event/create/",
        data: payload.id
          ? {
              module,
              room,
              teacher,
              topic
            }
          : {
              group: payload.group,
              date: payload.date,
              interval: payload.interval,
              module,
              room,
              teacher,
              topic
            }
      }

      const f = new FormData()
      Object.keys(req.data).forEach(key => f.append(key, req.data[key]))

      return this.$axios[req.type](req.url, f).then(res => {
        if (res) {
          // console.log("res", res)
          if (res.isAxiosError) {
            const { data } = res.response
            // console.error("An error occured", data)
            return {
              errors: data,
              interval: payload.interval
            }
          }

          return res.data || {}
        }
      })
    } catch (err) {
      // throw new Error(err);
    }
  }
}

export { actions }
