const actions = {
  getRoomsList(_, params) {
    return this.$axios.get("/schedule/room/list/", { params }).then(res => res?.data?.results || [])
  }
}

export { actions }
