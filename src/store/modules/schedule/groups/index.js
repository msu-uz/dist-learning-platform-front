// export const state = () => ({});

// export const mutations = {
//   setState(state, payload) {
//     Object.keys(payload).forEach((key) => {
//       if (key in state) {
//         state[key] = payload[key];
//       }
//     });
//   },
// };

export const actions = {
  fetchGroups(_, params) {
    return this.$axios.get("/schedule/studentgroup/list/", { params })
  },

  fetchDetail(_, { groupId, params = {} }) {
    // console.log("fetchDetail", groupId, params);
    return this.$axios.get(`/schedule/studentgroup/${groupId}/event/list/`, {
      params
    })
  },

  patchGroupCourse(_, { id, ...props }) {
    // const f = new FormData();
    // Object.keys(props).forEach((item) => {
    //   f.append(item, props[item]);
    // });
    return this.$axios.put(`/schedule/studentgroup/${id}/update/`, props)
  }
}
