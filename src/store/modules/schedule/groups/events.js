function toFormData(props) {
  const f = new FormData()

  Object.keys(props).forEach(key => {
    f.append(key, props[key])
  })

  return f
}

const actions = {
  createSchedule(_, { groupId, form }) {
    const data = toFormData(form)
    return this.$axios.post(`/schedule/studentgroup/${groupId}/event/create/`, data)
  },

  createScheduleRepeat(_, { groupId, form }) {
    const data = toFormData(form)
    return this.$axios.post(`/schedule/studentgroup/${groupId}/event/create/repeat/`, data)
  },

  getTeachersByGroupId(_, { groupId, params }) {
    return this.$axios
      .get(`/schedule/studentgroup/${groupId}/teacher/list/`, {
        params
      })
      .then(res => {
        // if (res && res.data) {
        // commit("setState", res.data || []);
        return res?.data || []
        // }
      })
  },

  getTeachersList(_, params) {
    return this.$axios
      .get(`/schedule/teacher/list/`, {
        params
      })
      .then(res => {
        // console.log('getTeachersList', res);
        // if (res && res.data) {
        // commit("setState", res.data || []);
        return res?.data?.results || []
        // }
      })
  },

  changeEvent(_, { eventId, data }) {
    // console.log("changeEvent", eventId, data);
    return this.$axios.put(`/schedule/event/${eventId}/change/`, data).then(res => res?.data || {})
  },

  swapEvent(_, { eventId, data }) {
    // console.log("swapEvent", eventId, data);
    return this.$axios.put(`/schedule/event/${eventId}/swap/`, data).then(res => res?.data || {})
  },

  setAudit(_, { groupId, eventId, roomId }) {
    return this.$axios.patch(`/schedule/studentgroup/${groupId}/event/${eventId}/update/`, {
      room: roomId
    })
  }
}

export { actions }
