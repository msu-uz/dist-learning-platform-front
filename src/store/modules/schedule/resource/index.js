const actions = {
  getResourceList(_, params) {
    return this.$axiosg
      .get("/schedule/resource/list/", {
        params
      })
      .then(res => res?.data || [])
  }
}

export { actions }
