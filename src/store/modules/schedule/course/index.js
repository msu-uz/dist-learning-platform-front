export const state = () => ({
  results: [],
  count: 0
})

export const mutations = {
  setState(state, payload) {
    Object.keys(payload).forEach(key => {
      if (key in state) {
        state[key] = payload[key]
      }
    })
  }
}

export const actions = {
  fetchList({ commit }, params) {
    return this.$axios
      .get("/schedule/course/list/", {
        params
      })
      .then(res => {
        if (res?.data) {
          commit("setState", res.data)
        }

        return res?.data?.results || []
      })
  }
}
