export const state = () => ({
  count: 0,
  results: [],
  admitted: {},
  userFields: {},
  id: null
})

export const mutations = {
  setState(state, payload) {
    Object.keys(payload).forEach(key => {
      if (key in state) {
        state[key] = payload[key]
      }
    })
  },
  removeState(state, id) {
    const f = state.results.findIndex(item => item.id == id)
    if (f != -1) {
      state.results = [...state.results.slice(0, f), ...state.results.slice(f + 1)]
    }
  }
}
export const actions = {
  //Applicants
  getApplicantList({ commit }, { id, params }) {
    return this.$axios
      .get(`/admission_board/results-list/${id}/applicants/list/`, {
        params
      })
      .then(res => {
        if (res && res.data) {
          commit("setState", res.data)
          console.log("getApplicantList", res.data)
        }
      })
  },

  getDetailResultsList({ commit }, params) {
    return this.$axios
      .get("/admission_board/results-list/current/", {
        params
      })
      .then(res => {
        if (res && res.data) {
          commit("setState", {
            admitted: res.data
          })
          console.log("getDetailResultsList", res.data)
        }
      })
  },

  approveResultList(_, id) {
    return this.$axios.post(`/admission_board/results-list/${id}/approve/`).then(res => {
      if (res && res.data) {
        console.log("Approving result list was done successfully")
      }
    })
  },
  removeApplicant({ commit }, { id, applicant_id }) {
    return this.$axios.delete(`/admission_board/results-list/${id}/remove/${applicant_id}/`).then(() => commit("removeState", applicant_id))
  }
}
