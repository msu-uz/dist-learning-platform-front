export const actions = {
  getStudents(_, params) {
    return this.$axios.get(`/admin/student/list/`, { params }).then(res => (res && res.data) || [])
  },
  getApplicants(_, params) {
    return this.$axios.get(`admin/student/applicant/list/`, { params }).then(res => (res && res.data) || [])
  },
  getCourseTypes(_, params) {
    return this.$axios.get(`/admin/coursetype/list/`, { params }).then(res => (res && res.data) || [])
  },
  getApplicant(_, id) {
    return this.$axios.get(`/admin/student/applicant/${id}/`).then(res => (res && res.data) || {})
  },
  createStudent(_, form) {
    return this.$axios.post(`/admin/student/applicant/student/create/`, form).then(res => res && res.data)
  }
}
