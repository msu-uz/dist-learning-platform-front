const model = "/catalog/e_kadr/"

export const actions = {
  getEmployeeList(_, params) {
    return this.$axios.get(`${model}employee/list/`, { params }).then(res => {
      console.log(params, res)
      if (res && res.data) {
        return res.data
      }
      return []
    })
  },

  getEmployeeById(_, id) {
    return this.$axios.get(`${model}employee/${id}/`).then(res => {
      return res.data
    })
  },

  getPersonnelList(_, params) {
    return this.$axios.get(`${model}ekadr/list/`, { params }).then(res => {
      console.log(params, res)
      if (res && res.data) {
        return res.data
      }
      return []
    })
  },

  create(_, { key, payload }) {
    return this.$axios
      .post(`${model}${key}/create/`, payload)
      .then(res => {
        // console.log("REQ", res)
        return res
      })
      .catch(err => {
        console.log("Err", err)
        return err
      })
  }
}
