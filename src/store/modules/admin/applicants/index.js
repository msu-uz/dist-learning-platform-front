export const actions = {
  getApplicantsList(_, params) {
    return this.$axios.get(`/admin/student/applicant/list/`, { params }).then(res => (res && res.data) || [])
  },
  getApplicant(_, id) {
    return this.$axios.get(`/admin/student/applicant/${id}/`).then(res => (res && res.data) || {})
  },
  createOrUpdate(_, form) {
    const urlBase = "/admin/student/applicant/"

    const req = {
      method: form.id ? "put" : "post",
      url: urlBase + ((form.id && `${form.id}/`) || "create/")
    }

    const f = new FormData()

    Object.keys(form).forEach(key => {
      if (["mother_info", "father_info"].includes(key)) {
        f.append(key, JSON.stringify(form[key]))
      } else {
        f.append(key, form[key])
      }
    })

    return this.$axios[req.method](req.url, f)
  }
}
