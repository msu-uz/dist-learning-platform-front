import {
  STUDENT_LEVEL,
  TEACHER_LEVEL,
  ADMISSION_BOARD_LEVEL,
  EDUCATION_OFFICE_UNITS_LEVEL,
  ADMINISTRATOR_LEVEL,
  LIBRARIAN_LEVEL
} from "@/constants/access_levels"

export default {
  [STUDENT_LEVEL]: {
    routes: [
      {
        name: "cabinetStudentsMain",
        title: { ru: "Главная", uz: " Asosiy", en: "Home" },
        icon: "student-dashbord"
      },
      {
        name: "cabinetStudentsSchedule",
        title: { ru: "Расписание", uz: "Jadval", en: "Schedule" },
        icon: "student-calendar"
      },

      {
        name: "cabinetStudentsLearning",
        title: { ru: "Обучение", uz: "Ta'lim", en: "Training" },
        icon: "student-education"
      },

      {
        name: "cabinetStudentsResults",
        title: { ru: "Результаты обучения", uz: "Ta'lim natijalari", en: "Learning results" },
        icon: "student-review"
      },
      {
        name: "cabinetStudentsLibrary",
        title: { ru: "Библиотека", uz: "Kutubxona", en: "Library" },
        icon: "student-bookmark"
      },

      // {ru: "", uz: "", en: ""}
      {
        name: "cabinetStudentsVideolib",
        title: { ru: "Видеоконференция", uz: "Videokonferentsiya", en: "Video conference" },
        icon: "teacher-clapperboard"
      },

      {
        name: "cabinetStudentsNotifications",
        title: { ru: "Уведомления", uz: "Bildirishnomalar", en: "Notifications" },
        icon: "teacher-bell"
      }
    ]
  },

  [TEACHER_LEVEL]: {
    routes: [
      {
        name: "cabinetTeachersMaterials",
        title: { ru: "Материалы", uz: "Materiallar", en: "Materials" },
        icon: "teacher-add"
      },

      {
        name: "cabinetTeachersGroups",
        title: { ru: "Группы", uz: "Guruhlar", en: "Groups" },
        icon: "teacher-group"
      },

      {
        name: "cabinetTeachersSchedule",
        title: { ru: "Расписание", uz: "Dars jadvali", en: "Schedule" },
        icon: "teacher-calendar"
      },

      {
        name: "cabinetTeachersRegister",
        title: { ru: "Ведомость", uz: "Bayonotlar", en: "Statement" },
        icon: "teacher-pen"
      },
      {
        name: "cabinetTeachersLibrary",
        title: { ru: "Библиотека", uz: "Kutubxona", en: "Library" },
        icon: "teacher-bookmark"
      },

      // {
      //   name: "cabinetTeachersVideolib",
      //   title: "Видео-библиотека",
      //   icon: "teacher-clapperboard"
      // },

      {
        name: "cabinetTeachersTasks",
        title: { ru: "Рассылка заданий", uz: "Vazifalarni yuborish", en: "Sending out tasks" },
        icon: "teacher-list"
      },
      {
        name: "cabinetTeachersCheckTasks",
        title: { ru: "Проверка заданий", uz: "Ishlarni tekshirish", en: "Checking tasks" },
        icon: "student-review"
      },

      {
        name: "cabinetTeachersNotifications",
        title: { ru: "Уведомления", uz: "Bildirishnomalar", en: "Notifications" },
        icon: "teacher-bell"
      },
      {
        name: "cabinetTeachersFeedback",
        title: { ru: "Обратная связь", uz: "Qayta aloqa", en: "Feedback" },
        icon: "teacher-shout"
      },
      {
        name: "cabinetTeachersVideoconf",
        title: { ru: "Видеоконференция", uz: "Videokonferentsiya", en: "Video conference" },
        icon: "teacher-clapperboard"
      },
      {
        name: "cabinetTeachersTests",
        title: { ru: "База тестов", uz: "Sinov bazasi", en: "Test database" },
        icon: "book"
      },
      // {ru: "", uz: "", en: ""}
      {
        name: "cabinetTeachersTestsStatistics",
        title: { ru: "Статистика тестов", uz: "Test statistikasi", en: "Test statistics" },
        icon: "statistics"
      }
    ]
  },

  [ADMISSION_BOARD_LEVEL]: {
    routes: [
      {
        name: "cabinetCommissionStatements",
        title: "Заявления",
        icon: "add"
      },
      {
        name: "cabinetCommissionTest",
        title: "Результаты",
        icon: "teacher-review"
      },
      {
        name: "cabinetCommissionAdmitted",
        title: "Поступившие",
        icon: "tick"
      }
    ]
  },

  [EDUCATION_OFFICE_UNITS_LEVEL]: {
    routes: [
      {
        name: "cabinetLearningLearningProgram",
        title: { ru: "Программа обучения", uz: " O'quv dasturi", en: "Materials" },
        icon: "student-education"
      },
      {
        name: "cabinetLearningGroups",
        title: { ru: "Список групп", uz: "Guruhlar ro'yxati", en: "Groups" },
        icon: "teacher-group"
      },
      {
        name: "cabinetLearningSchedule",
        title: { ru: "Расписание", uz: "Dars jadvali", en: "Schedule" },
        icon: "student-calendar"
      },
      {
        name: "cabinetLearningTeachers",
        title: "Преподаватели",
        icon: "user"
      },
      {
        name: "cabinetLearningBurden",
        title: "Нагрузка",
        icon: "time"
      },
      {
        name: "cabinetLearningAttendance",
        title: "Посещаемость",
        icon: "tick"
      },
      {
        name: "cabinetLearningRegister",
        title: "Ведомости",
        icon: "teacher-pen"
      },
      {
        name: "cabinetLearningConnection",
        title: "Обратная связь",
        icon: "teacher-shout"
      },
      {
        name: "cabinetLearningNotifications",
        title: "Уведомления",
        icon: "student-bell"
      },
      {
        name: "cabinetLearningResources",
        title: "Ресурсы",
        icon: "student-bookmark"
      }
    ]
  },

  [ADMINISTRATOR_LEVEL]: {
    routes: [
      {
        name: "cabinetAdminCatalog",
        title: "Справочник",
        icon: "directory"
      },
      {
        name: "cabinetAdminPersonnel",
        title: "Кадры",
        icon: "user"
      }
    ]
  },

  [LIBRARIAN_LEVEL]: {
    routes: [
      {
        name: "cabinetLibraryLibrary",
        title: "Библиотека",
        icon: "directory"
      }
    ]
  }
}
