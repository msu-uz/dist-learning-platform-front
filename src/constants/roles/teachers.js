export default [
  [
    {
      name: "cabinetTeachersMaterials",
      title: { ru: "Материалы", uz: "Materiallar", en: "Materials" },
      icon: "teacher-add"
    },

    {
      name: "cabinetTeachersGroups",
      title: { ru: "Группы", uz: "Guruhlar", en: "Groups" },
      icon: "teacher-group"
    },

    {
      name: "cabinetTeachersSchedule",
      title: { ru: "Расписание", uz: "Dars jadvali", en: "Schedule" },
      icon: "teacher-calendar"
    },

    {
      name: "cabinetTeachersRegister",
      title: { ru: "Ведомость", uz: "Bayonotlar", en: "Statement" },
      icon: "teacher-pen"
    }
  ],

  [
    {
      name: "cabinetTeachersLibrary",
      title: { ru: "Библиотека", uz: "Kutubxona", en: "Library" },
      icon: "teacher-bookmark"
    },

    // {
    //   name: "cabinetTeachersVideolib",
    //   title: "Видео-библиотека",
    //   icon: "teacher-clapperboard"
    // },

    {
      name: "cabinetTeachersTasks",
      title: { ru: "Рассылка заданий", uz: "Vazifalarni yuborish", en: "Sending out tasks" },
      icon: "teacher-list"
    },

    {
      name: "cabinetTeachersCheckTasks",
      title: { ru: "Проверка заданий", uz: "Ishlarni tekshirish", en: "Checking tasks" },
      icon: "student-review"
    },

    {
      name: "cabinetTeachersNotifications",
      title: { ru: "Уведомления", uz: "Bildirishnomalar", en: "Notifications" },
      icon: "teacher-bell"
    }
  ],

  [
    {
      name: "cabinetTeachersFeedback",
      title: { ru: "Обратная связь", uz: "Qayta aloqa", en: "Feedback" },
      icon: "teacher-shout"
    },
    {
      name: "cabinetTeachersTests",
      title: { ru: "База тестов", uz: "Sinov bazasi", en: "Test database" },
      icon: "book"
    },
    {
      name: "cabinetTeachersTestsStatistics",
      title: { ru: "Статистика тестов", uz: "Test statistikasi", en: "Test statistics" },
      icon: "statistics"
    }
  ]
]
