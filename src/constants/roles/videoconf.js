import { state } from "@/store/modules/profile/index"

console.log(state().fields)
// export default state.fields.access_level == 1
//   ? [
//       [
//         {
//           name: "cabinetStudentsMain",
//           title: "Главная",
//           icon: "student-dashbord"
//         }
//       ],
//
//       [
//         {
//           name: "cabinetStudentsSchedule",
//           title: "Расписание",
//           icon: "student-calendar"
//         },
//
//         {
//           name: "cabinetStudentsLearning",
//           title: "Обучение",
//           icon: "student-education"
//         },
//
//         {
//           name: "cabinetStudentsResults",
//           title: "Результаты обучения",
//           icon: "student-review"
//         }
//       ],
//
//       [
//         {
//           name: "cabinetStudentsLibrary",
//           title: "Библиотека",
//           icon: "student-bookmark"
//         },
//
//         {
//           name: "cabinetStudentsVideolib",
//           title: "Видео материалы",
//           icon: "teacher-clapperboard"
//         },
//
//         {
//           name: "cabinetStudentsNotifications",
//           title: "Уведомления",
//           icon: "teacher-bell"
//         },
//         {
//           name: "cabinetVideoconf",
//           title: "Видеоконференция",
//           icon: "teacher-clapperboard"
//         }
//       ]
//     ]
//   : [
//       [
//         {
//           name: "cabinetTeachersMaterials",
//           title: "Материалы",
//           icon: "teacher-add"
//         },
//
//         {
//           name: "cabinetTeachersGroups",
//           title: "Группы",
//           icon: "teacher-group"
//         },
//
//         {
//           name: "cabinetTeachersSchedule",
//           title: "Расписание",
//           icon: "teacher-calendar"
//         },
//
//         {
//           name: "cabinetTeachersRegister",
//           title: "Ведомость",
//           icon: "teacher-pen"
//         }
//       ],
//
//       [
//         {
//           name: "cabinetTeachersLibrary",
//           title: "Библиотека",
//           icon: "teacher-bookmark"
//         },
//
//         {
//           name: "cabinetTeachersVideolib",
//           title: "Видео-библиотека",
//           icon: "teacher-clapperboard"
//         },
//
//         {
//           name: "cabinetTeachersTasks",
//           title: "Рассылка заданий",
//           icon: "teacher-list"
//         },
//
//         {
//           name: "cabinetTeachersCheckTasks",
//           title: "Проверка заданий",
//           icon: "student-review"
//         },
//
//         {
//           name: "cabinetTeachersNotifications",
//           title: "Уведомления",
//           icon: "teacher-bell"
//         }
//       ],
//
//       [
//         {
//           name: "cabinetTeachersFeedback",
//           title: "Обратная связь",
//           icon: "teacher-shout"
//         },
//         {
//           name: "cabinetVideoconf",
//           title: "Видеоконференция",
//           icon: "teacher-clapperboard"
//         }
//       ]
//     ];
