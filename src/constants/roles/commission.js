export default [
  [
    {
      name: "cabinetCommissionStatements",
      title: { ru: "Заявления", uz: " Arizalar", en: "Applications" },
      icon: "add"
    },
    {
      name: "cabinetCommissionTest",
      title: { ru: "Результаты", uz: "Natijalar", en: "Results" },
      icon: "teacher-review"
    },
    {
      name: "cabinetCommissionAdmitted",
      title: { ru: "Поступившие", uz: "Qabul qilingan", en: "Enrolled" },
      icon: "tick"
    }
  ]
]
