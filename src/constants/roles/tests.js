export default [
  [
    {
      name: "cabinetTestsModules",
      title: { ru: "Модулы", uz: "Modullar", en: "Modules" },
      icon: ""
    },

    {
      name: "cabinetTestsIncidents",
      title: { ru: "Казусы", uz: "Savollar", en: "Incidents" },
      icon: ""
    },

    {
      name: "cabinetTestsQuestions",
      title: { ru: "Вопросы", uz: "Savollar va javoblar", en: "Questions and answers" },
      icon: ""
    },

    {
      name: "cabinetTestsAnalytics",
      title: { ru: "Аналитика", uz: "Tahlil", en: "Analytics" },
      icon: ""
    }
  ]
]
