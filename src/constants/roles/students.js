export default [
  [
    {
      name: "cabinetStudentsMain",
      title: { ru: "Главная", uz: " Asosiy", en: "Home" },
      icon: "student-dashbord"
    }
  ],

  [
    {
      name: "cabinetStudentsSchedule",
      title: { ru: "Расписание", uz: "Jadval", en: "Schedule" },
      icon: "student-calendar"
    },

    {
      name: "cabinetStudentsLearning",
      title: { ru: "Обучение", uz: "Ta'lim", en: "Training" },
      icon: "student-education"
    },

    {
      name: "cabinetStudentsResults",
      title: { ru: "Результаты обучения", uz: "Ta'lim natijalari", en: "Learning results" },
      icon: "student-review"
    }
  ],

  [
    {
      name: "cabinetStudentsLibrary",
      title: { ru: "Библиотека", uz: "Kutubxona", en: "Library" },
      icon: "student-bookmark"
    },

    // {
    //   name: "cabinetStudentsVideolib",
    //   title: "Видео материалы",
    //   icon: "teacher-clapperboard"
    // },

    {
      name: "cabinetStudentsNotifications",
      title: { ru: "Уведомления", uz: "Bildirishnomalar", en: "Notifications" },
      icon: "teacher-bell"
    },
    {
      name: "cabinetStudentsFeedback",
      title: { ru: "Обратная связь", uz: "Qayta aloqa", en: "Feedback" },
      icon: "teacher-shout"
    }
  ]
]
