export default [
  [
    {
      name: "cabinetAdminCatalog",
      title: { ru: "Справочник", uz: "Katalog", en: "Catalog" },
      icon: "directory"
    },
    {
      name: "cabinetAdminPersonnel",
      title: { ru: "Кадры", uz: "Xodimlar", en: "Staff" },
      icon: "staff"
    },
    {
      name: "cabinetAdminApplicants",
      title: { ru: "Аппликанты", uz: "Nomzodlar", en: "Applicants" },
      icon: "staff_add"
    },
    {
      name: "cabinetAdminStudents",
      title: { ru: "Студенты", uz: "Talabalar", en: "Students" },
      icon: "student"
    }
  ]
]
