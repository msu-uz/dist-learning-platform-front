export default [
  [
    {
      name: "cabinetLearningLearningProgram",
      //
      title: { ru: "Программа обучения", uz: " O'quv dasturi", en: "Materials" },
      icon: "student-education"
    },
    {
      name: "cabinetLearningGroups",
      title: { ru: "Список групп", uz: "Guruhlar ro'yxati", en: "Groups" },
      icon: "teacher-group"
    },
    {
      name: "cabinetLearningSchedule",
      title: { ru: "Расписание", uz: "Dars jadvali", en: "Schedule" },
      icon: "student-calendar"
    }
  ],
  [
    {
      name: "cabinetLearningTeachers",
      title: { ru: "Преподаватели", uz: "O'qituvchilar", en: "Statement" },
      icon: "user"
    },
    {
      name: "cabinetLearningLoad",
      //
      title: { ru: "Нагрузка", uz: "Yuklanish", en: "Load" },
      icon: "time"
    }
  ],
  [
    {
      name: "cabinetLearningAttendance",
      title: { ru: "Посещаемость", uz: "Davomat", en: "Attendance " },
      icon: "tick"
    },
    {
      name: "cabinetLearningRegister",
      title: { ru: "Ведомости", uz: "Bayonotlar", en: "Statements" },
      icon: "teacher-pen"
    },
    //  {ru: "", uz: "", en: ""}
    {
      name: "cabinetLearningConnection",
      title: { ru: "Обратная связь", uz: "Qayta aloqa", en: "Feedback" },
      icon: "teacher-shout"
    },
    {
      name: "cabinetLearningNotifications",
      title: { ru: "Уведомления", uz: "Bildirishnomalar", en: "Notifications" },
      icon: "student-bell"
    },
    {
      name: "cabinetLearningResources",
      title: { ru: "Ресурсы", uz: "Manbaalar", en: "Resources" },
      icon: "student-bookmark"
    }
  ]
]
