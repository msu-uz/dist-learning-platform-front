export const STUDENT_LEVEL = 1
export const TEACHER_LEVEL = 2
export const ADMISSION_BOARD_LEVEL = 3
export const EDUCATION_OFFICE_UNITS_LEVEL = 4
export const ADMINISTRATOR_LEVEL = 5
export const LIBRARIAN_LEVEL = 6
