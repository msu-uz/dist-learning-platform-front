import Vue from "vue"
import App from "./App.vue"
import router from "./router"
import store from "./store"
import vSelect from "vue-select"
import VueTimepicker from "vue2-timepicker"
import PerfectScrollbar from "vue2-perfect-scrollbar"
import Vuelidate from "vuelidate"
import "./assets/scss/base/_mixins.scss"
import "./assets/scss/main.scss"
import "./assets/fonts/fonts.css"
import "bulma-calendar/dist/css/bulma-calendar.min.css"
import "vue2-perfect-scrollbar/dist/vue2-perfect-scrollbar.css"
import SortedTablePlugin from "vue-sorted-table"
import { isJSON } from "@/plugins/utils/json-checker"
import { isObject } from "@/plugins/utils"
import { optionalChaining } from "@/plugins/utils/optional-chaining"
import i18n, { nt } from "@/plugins/i18n"
import VueDragscroll from "vue-dragscroll"

import infiniteScroll from "vue-infinite-scroll"
Vue.use(infiniteScroll)

Vue.use(VueDragscroll)

Vue.use(SortedTablePlugin)
Vue.use(PerfectScrollbar)
Vue.use(Vuelidate)
import moment from "moment"
import momentTZ from "moment-timezone"
// console.log("i18n", i18n);
// moment.locale(i18n.locale || "ru")
moment.locale("ru")

momentTZ.tz.setDefault("Asia/Tashkent")
moment().format()

Vue.prototype.FILE_URL = process.env.VUE_APP_FILE_URL
Vue.prototype.$API_URL = process.env.VUE_APP_API_URL
Vue.prototype.isJSON = isJSON
// console.log(isJSON("{ \"name\": \"name_uz\"}"))   => true;

Vue.prototype.$$ = optionalChaining
Vue.prototype.$gv = function(value) {
  if (isObject(value)) {
    const t = nt(value)
    return t
  }

  return value
}
Vue.prototype.$fn = function(array) {
  return `${array.first_name || ""} ${array.middle_name || array.second_name || ""} ${array.last_name || ""}`
}

import PreDraggable from "@/components/PreDraggable"

Vue.component("PreDraggable", PreDraggable)
Vue.component("v-select", vSelect)
Vue.component("v-timepicker", VueTimepicker)
const bulmaCalendar = require("bulma-calendar/dist/js/bulma-calendar.min.js")

// CSS
import "vue2-timepicker/dist/VueTimepicker.css"

Vue.prototype.$bulmaCalendar = bulmaCalendar
Vue.prototype.$moment = moment

import { BadgerAccordion, BadgerAccordionItem } from "vue-badger-accordion"

Vue.component("BadgerAccordion", BadgerAccordion)
Vue.component("BadgerAccordionItem", BadgerAccordionItem)

// optionally import default styles
import "vue-draggable-resizable/dist/VueDraggableResizable.css"

Vue.config.productionTip = false

new Vue({
  i18n,
  router,
  store,
  render: h => h(App),
  created() {
    // // // // // // // // // // // //
    //  Axios unknown error handler  //
    // // // // // // // // // // // //
    document.addEventListener("response-unknown-error", function(e) {
      if (e.detail.name == -1) {
        store.dispatch("snackbar/changeToasts", {
          text: `Возникла непредвиденная ошибка`,
          color: "danger"
        })
      }
    })
  }
}).$mount("#app")
