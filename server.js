const compression = require("compression")
const express = require("express")
const serveStatic = require("serve-static")

let app = express()

app.use(compression())

app.use(serveStatic(__dirname + "/dist"))

app.use("/", express.static(__dirname + "/dist"))

app.get("*", (req, res) => {
  return res.sendFile(__dirname + "/dist/index.html")
})

const port = process.env.PORT || 5000
app.listen(port, () => {
  console.log("Listening on port " + port)
})
