const path = require("path")

const isDev = process.env.NODE_ENV !== "production"

module.exports = {
  pluginOptions: {
    // i18n
    i18n: {
      locale: process.env.VUE_APP_I18N_LOCALE || "uz",
      fallbackLocale: process.env.VUE_APP_I18N_FALLBACK_LOCALE || "uz",
      localeDir: process.env.VUE_APP_I18N_LOCALE_DIR || "locales",
      enableInSFC: true
    },

    compression: {
      brotli: {
        filename: "[path].br[query]",
        algorithm: "brotliCompress",
        include: /\.(js|css|html|svg|json)(\?.*)?$/i,
        compressionOptions: {
          level: 11
        },
        minRatio: 0.8
      },
      gzip: {
        filename: "[path].gz[query]",
        algorithm: "gzip",
        include: /\.(js|css|html|svg|json)(\?.*)?$/i,
        minRatio: 0.8
      }
    }
  },

  publicPath: "/",
  lintOnSave: isDev,
  productionSourceMap: false,

  configureWebpack: {
    resolve: {
      alias: {
        // If using the runtime only build
        vue$: 'vue/dist/vue.runtime.esm.js' // 'vue/dist/vue.runtime.common.js' for webpack 1
        // Or if using full build of Vue (runtime + compiler)
        // vue$: 'vue/dist/vue.esm.js'      // 'vue/dist/vue.common.js' for webpack 1
      }
    }
  },

  chainWebpack(config) {
    config.resolve.alias.set("@", path.join(__dirname, "./src"))

    const svgRule = config.module.rule("svg")

    svgRule.uses.clear()

    svgRule
      .oneOf("inline")
      .resourceQuery(/inline/)
      .use("babel-loader")
      .loader("babel-loader")
      .end()
      .use("vue-svg-loader")
      .loader("vue-svg-loader")
      .end()
      .end()
      .oneOf("external")
      .use("file-loader")
      .loader("file-loader")
      .options({
        name: "assets/[name].[hash:8].[ext]"
      })
  },

  css: {
    loaderOptions: {
      postcss: {
        config: {
          path: __dirname
        }
      },
      scss: {
        prependData: `@import "@/assets/scss/base/_variables.scss";`
      }
    }
  }
}
