module.exports = {
  trailingComma: "none",
  semi: false,
  useTabs: false,
  printWidth: 150
}
